/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko11;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 *
 * @author Lassi Nieminen
 *          IT-Kesäleiri 2018 Olio-ohjelmointia
 */
public class FXMLDocumentController implements Initializable {
    
    ShapeHandler ShaperInstance = ShapeHandler.getShapeHandlerInstance();
    @FXML
    private AnchorPane basePane;
    
    private Point clickedPoint = null;
    //EventType<MyEvent> OPTIONS_ALL = new EventType<>("OPTIONS_ALL");
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    

    @FXML
    private void AddCircleAction(MouseEvent event) {
        
        if (event.getButton() == MouseButton.SECONDARY) {
            System.out.println("Right Click");
            clickedPoint = null;
            if (event.getTarget() instanceof Circle) {
                ShaperInstance.deletePoint(event.getTarget());
                Object deviantLine = ShaperInstance.killCheck();
                while (deviantLine != null) {
                    basePane.getChildren().remove(deviantLine);
                    deviantLine = ShaperInstance.killCheck();
                }
            } else if (event.getTarget() instanceof Line) {
                ShaperInstance.deleteLine(event.getTarget());
            }
                
            basePane.getChildren().remove(event.getTarget());
            
        } else if (!(event.getTarget() instanceof Circle)){
            ShaperInstance.addPoint(event.getX(), event.getY());
            basePane.getChildren().add(ShaperInstance.getLastPoint());
            clickedPoint = null;
            
            
        } else {
            
            if (clickedPoint == null){
                clickedPoint = ShaperInstance.getPoint(event.getTarget());
                
                
            } else {
                if (ShaperInstance.getPoint(event.getTarget()) != clickedPoint) {
                    ShaperInstance.addLine(clickedPoint.getPallo(),(Circle)event.getTarget());
                    basePane.getChildren().add(0, ShaperInstance.getLastViiva());
                    clickedPoint = null;
                }
            }
            System.out.println(clickedPoint);
        }
    }
    
}
