/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package viikko11;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 *
 * @author Lassi Nieminen
 *          IT-Kesäleiri 2018 Olio-ohjelmointia
 */
public class Viiva {
    private final Circle alkuPallo;
    private final Circle loppuPallo;
    private Line viiva;
    
    public Viiva(Circle alku, Circle loppu) {
        alkuPallo = alku;
        loppuPallo = loppu;
        viiva = new Line(alkuPallo.getCenterX(), alkuPallo.getCenterY(), loppuPallo.getCenterX(), loppuPallo.getCenterY());
        viiva.strokeWidthProperty().setValue(3);
        viiva.strokeProperty().setValue(Color.RED);
    }
    
    public int deathCheck(){
        if (alkuPallo == null || loppuPallo == null) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public Line getLine(){
        return viiva;
    }
    
    public Circle getAlkuPallo() {
        return alkuPallo;
    }
    
    public Circle getLoppuPallo() {
        return loppuPallo;
    }

}
