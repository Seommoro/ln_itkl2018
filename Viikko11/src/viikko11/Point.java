/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package viikko11;

import javafx.event.EventHandler;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 *
 * @author Lassi Nieminen
 *          IT-Kesäleiri 2018 Olio-ohjelmointia
 */
public class Point {
    private final double X;
    private final double Y;
    private Circle pallo;
    
    public Point(double x, double y) {
        X = x;
        Y = y;
        pallo = new Circle(X, Y, 10, Color.RED);
        pallo.setOnMouseClicked(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            if (event.getButton() == MouseButton.SECONDARY) {
                
            }
            //event.consume();
        }
        });
    }
    
    public double getX(){
        return X;
    }
    public double getY() {
        return Y;
    }
    
    public Circle getPallo(){
        return pallo;
    }
    

}
