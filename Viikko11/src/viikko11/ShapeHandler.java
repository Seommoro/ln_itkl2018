/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package viikko11;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 *
 * @author Lassi Nieminen
 *          IT-Kesäleiri 2018 Olio-ohjelmointia
 */
public class ShapeHandler {
    private final ArrayList<Point> palloLista;
    private final ArrayList<Viiva> viivaLista;
    private static ShapeHandler instanssi = new ShapeHandler();
    
    private ShapeHandler(){
        palloLista = new ArrayList();
        viivaLista = new ArrayList();
    }
    
    public static ShapeHandler getShapeHandlerInstance(){
        return instanssi;
    }
    
    public void addPoint(double X, double Y){
        palloLista.add(new Point(X, Y));
    }
    
    public Circle getLastPoint() {
        return palloLista.get(palloLista.size()-1).getPallo();
    }
    
    public Point getPoint(Object kohde){
        Iterator<Point> iteraattori = palloLista.iterator();
        while (iteraattori.hasNext()){
            Point k = iteraattori.next();
            if (k.getPallo() == kohde) {
                return k;
            }
        }
        return null;
    }
    public void deletePoint(Object kohde){
        Iterator<Point> iteraattori = palloLista.iterator();
        while (iteraattori.hasNext()){
            Point k = iteraattori.next();
            if (k.getPallo() == kohde) {
                iteraattori.remove();
            }
        }
        System.out.print("palloListan alkiot: ");
        System.out.println(palloLista);
    }
    
    public void addLine(Circle alku, Circle loppu) {
        viivaLista.add(new Viiva(alku, loppu));   
    }
    
    public Line getLastViiva() {
        return viivaLista.get(viivaLista.size()-1).getLine();
    }
    
    public Line killCheck(){
        int alkuFound;
        int loppuFound;
        NEWLINE:
        for (int i = 0; i < viivaLista.size(); i++) {
            alkuFound = 0;
            loppuFound = 0;
            for (int j = 0; j < palloLista.size(); j++) {
                if (viivaLista.get(i).getAlkuPallo() == palloLista.get(j).getPallo()) {
                    alkuFound = 1;
                }
                if (viivaLista.get(i).getLoppuPallo() == palloLista.get(j).getPallo()) {
                    loppuFound = 1;
                }
                if (alkuFound == 1 && loppuFound == 1) {
                    System.out.println("Found a ball");
                    continue NEWLINE;
                }
            }
            Line palautus = viivaLista.get(i).getLine();
            viivaLista.remove(i);
            System.out.print("viivaListan alkiot (killCheck()): ");
            System.out.println(viivaLista);
            return palautus;
        }
        System.out.println("All a-okay");
        return null;
    }
    public void deleteLine(Object kohde){
        Iterator<Viiva> iteraattori = viivaLista.iterator();
        while (iteraattori.hasNext()){
            Viiva k = iteraattori.next();
            if (k.getLine() == kohde) {
                iteraattori.remove();
            }
        }
        System.out.print("viivaListan alkiot (deleteLine()): ");
        System.out.println(viivaLista);
    }
}
