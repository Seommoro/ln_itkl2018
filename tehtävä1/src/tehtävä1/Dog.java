/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtävä1;

import java.util.Scanner;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
/**
 *
 * @author Lassi Nieminen
 */
public class Dog {
    private String nimi;
    private String sanoo;
  
    public Dog(String uNimi) {
        sanoo =  "Much wow!";
        if (uNimi.trim().isEmpty()) {
            nimi = "Doge";
        } else {
            nimi = uNimi;
        }
        System.out.println("Hei, nimeni on "+ nimi); 
    }
    
    public void Speak(String lause){
        Scanner scan = new Scanner(lause);
        while (scan.hasNext()){
            if(scan.hasNextBoolean()){
                System.out.println("Such boolean: " + scan.nextBoolean());
            } else if (scan.hasNextInt()) {
                System.out.println("Such integer: " + scan.nextInt());            
            } else {
                System.out.println(scan.next());
            }
        }
    }
}