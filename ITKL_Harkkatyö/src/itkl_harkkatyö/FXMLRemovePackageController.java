/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itkl_harkkatyö;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;

/**
 * FXML Controller class
 *
 * @author p9042
 */
public class FXMLRemovePackageController implements Initializable {

    
    GreatLeader greatLeader = GreatLeader.getInstance();
    @FXML
    private ComboBox<Package> packageList;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        packageList.setItems(greatLeader.getPackageList());
    }    

    @FXML
    private void removePackageAction(ActionEvent event) {
        if (packageList.getValue() != null) {
            greatLeader.removePackage(packageList.getValue().getPackageID());
            packageList.setItems(greatLeader.getPackageList());
        }
        
    }
    
}
