/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package itkl_harkkatyö;

/**
 *
 * @author Lassi Nieminen
 *          IT-Kesäleiri 2018 Olio-ohjelmointia
 */
public class PackageClass {
    private int classID;
    private int maxX;
    private int maxY;
    private int maxZ;
    private int maxWeight;
    private int maxDistance;
    private boolean willBreak;
    
    public PackageClass(int ID, int x, int y, int z, int weight, int dist, boolean breaks) {
        classID = ID;
        maxX = x;
        maxY = y;
        maxZ = z;
        maxWeight = weight;
        maxDistance = dist;
        willBreak = breaks;
    }
    
    public PackageClass(int ID, int dist, boolean breaks) {
        classID = ID;
        maxX = 0;
        maxY = 0;
        maxZ = 0;
        maxWeight = 0;
        maxDistance = dist;
        willBreak = breaks;
    }
    
    public int getID() {
        return classID;
    }
    
    public int getX(){
        return maxX;
    }
    public int getY(){
        return maxY;
    }
    public int getZ(){
        return maxZ;
    }
    public int getWeight(){
        return maxWeight;
    }
    public int getDistance(){
        return maxDistance;
    }

    public boolean isWillBreak() {
        return willBreak;
    }
    
    
    @Override
    public String toString() {
        String returnString = String.format("%d. Luokka", classID);
        return returnString;
    }
    

}
