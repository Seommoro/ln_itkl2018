/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itkl_harkkatyö;

/**
 *
 * @author Lassi
 */
public class Shipping {
    private int shippingID;
    private Package shippedPackage;
    private SmartPost startPost;
    private SmartPost endPost;
    private boolean isBroken;
    
    public Shipping(int shipID,Package newPackage, SmartPost start, SmartPost end) {
        shippingID = shipID;
        shippedPackage = newPackage;
        startPost = start;
        endPost = end;
        if (newPackage.getContents().getFragile() && newPackage.getPackageClass().isWillBreak()) {
            isBroken = true;
        } else {
            isBroken = false;
        }
            
    }
    
    @Override
    public String toString(){
        return String.format("ID: %d -- %s, FROM: %s, TO: %s. STATUS: %s", shippingID, shippedPackage.toString(), startPost.toString(), endPost.toString(), (isBroken ? "Hajonnut, asiakkaan vika; Ei korvata" : "Ei hajonnut"));
    }
}
