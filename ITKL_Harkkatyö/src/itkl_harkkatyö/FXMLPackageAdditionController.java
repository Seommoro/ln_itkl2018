/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itkl_harkkatyö;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.sqlite.util.StringUtils;

/**
 * FXML Controller class
 *
 * @author p9042
 */
public class FXMLPackageAdditionController implements Initializable {

    @FXML
    private ChoiceBox<PackageClass> classSelectionBox;
    @FXML
    private ComboBox<Item> itemSelectionBox;
    
    GreatLeader greatLeader = GreatLeader.getInstance();
    @FXML
    private TextField itemWidth;
    @FXML
    private TextField itemHeight;
    @FXML
    private TextField itemDepth;
    @FXML
    private TextField itemName;
    @FXML
    private TextField itemWeight;
    @FXML
    private CheckBox itemFragile;
    @FXML
    private CheckBox itemSalami;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        classSelectionBox.setItems(greatLeader.getPackageClassList());
        itemSelectionBox.setItems(greatLeader.getItemList());
        
    }    

    @FXML
    private void packageAddAction(ActionEvent event) {
        if ((itemSelectionBox.getValue() == null) || classSelectionBox.getValue() == null) {
            System.out.println("Item or package class has not been selected.");
        } else {
            PackageClass selectedClass = classSelectionBox.getValue();
            System.out.println(selectedClass.getID());
            Item selectedItem = itemSelectionBox.getValue();
            System.out.println(selectedItem.getWeight());
            System.out.println(selectedClass.getWeight());
            if (selectedItem.getX() <= selectedClass.getX() &&
                    selectedItem.getY() <= selectedClass.getY() &&
                    selectedItem.getZ() <= selectedClass.getZ() &&
                    selectedItem.getWeight()<= selectedClass.getWeight()) {
                greatLeader.addNewPackageToDatabase(selectedItem.getID(), selectedClass.getID());
            } else {
                System.out.println("Jokin valinta ei kelpaa");
            }
        }
    }

    @FXML
    private void cancelPackageAddAction(ActionEvent event) {
        ((Stage)((Button)event.getSource()).getScene().getWindow()).close();
    }

    @FXML
    private void addNewItemAction(ActionEvent event) {
        if(itemSalami.isSelected()) {
            System.out.println("5/5 maistuu salami");
        }
        if (isInteger(itemWidth.getText()) && isInteger(itemHeight.getText()) &&
            isInteger(itemDepth.getText()) && isInteger(itemWeight.getText()) && !itemName.getText().contains(";")&& !itemName.getText().contains("'")) {
                greatLeader.addNewItemToDatabase(Integer.parseInt(itemWidth.getText()), 
                Integer.parseInt(itemHeight.getText()), Integer.parseInt(itemDepth.getText()),
                itemName.getText(), Integer.parseInt(itemWeight.getText()),
                itemFragile.isSelected());
                itemSelectionBox.setItems(greatLeader.getItemList());
        } else {
            System.out.println("Esineen lisäys epäonnistui.");
        }
    }
    
    
    private boolean isInteger(String input) {
        try {
            int test = Integer.parseInt(input);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
}
