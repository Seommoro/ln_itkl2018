/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package itkl_harkkatyö;

/**
 *
 * @author Lassi Nieminen
 *          IT-Kesäleiri 2018 Olio-ohjelmointia
 */
public class SmartPost {
    private final int smartPostID;
    private final String name;
    private final String city;
    private final String address;
    private final String postalcode;
    private final String availability;
    private final String lat;
    private final String lng;
    
    public SmartPost(int ID, String n, String c, String a, String p, String av, String la, String ln) {
        smartPostID = ID;
        name = n;
        city = c;
        address = a;
        postalcode = p;
        availability = av;
        lat = la;
        lng = ln;
    }
    
    public SmartPost(String n) {
        this(0, n, "", "", "", "", "", "");
    }
    
    
    @Override
    public String toString() {
        return name;
    }
    
    public int getSmartPostID() {
        return smartPostID;
    }
    
    public String getName() {
        return name;
    }
    public String getCity() {
        return city;
    }
    public String getAddress() {
        return address;
    }
    public String getPostalCode() {
        return postalcode;
    }
    public String getAvailability() {
        return availability;
    }
    public String getLat() {
        return lat;
    }
    public String getLng() {
        return lng;
    }

}
