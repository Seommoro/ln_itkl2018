/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itkl_harkkatyö;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author p9042
 */
public class FXMLDocumentController implements Initializable {

    private Label label;
    @FXML
    private WebView mapBox;
    @FXML
    private ComboBox<String> cityList;

    GreatLeader greatLeader = GreatLeader.getInstance();
    @FXML
    private ComboBox<Package> packetList;
    @FXML
    private ComboBox<String> startCityList;
    @FXML
    private ComboBox<SmartPost> startSmartPost;
    @FXML
    private ComboBox<String> endCityList;
    @FXML
    private ComboBox<SmartPost> endSmartPost;
    @FXML
    private TextArea sendStatusLabel;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        mapBox.getEngine().load(getClass().getResource("index.html").toExternalForm());
        System.out.println(System.getProperty("user.dir") + "\\HT.db");
        File tmpDir = new File(System.getProperty("user.dir") + "\\HT.db");
        System.out.println(tmpDir.exists());
        if (!tmpDir.exists()) {
            greatLeader.TrumpWontLikeThis(false);
            greatLeader.initializeSmartPost();
        }
        cityList.setItems(greatLeader.getCityList());
        startCityList.setItems(greatLeader.getCityList());
        endCityList.setItems(greatLeader.getCityList());
    }

    @FXML
    private void showSmartPostAction(ActionEvent event) {
        String[] commandList = greatLeader.getMapMarkerCommands(cityList.getValue());
        String scriptCall;
        for (String s : commandList) {
            scriptCall = String.format("document.goToLocation(%s, 'red')", s);
            System.out.println(scriptCall);
            mapBox.getEngine().executeScript(scriptCall);
        }

    }

    @FXML
    private void updateSmartPostAction(ActionEvent event) {
        greatLeader.initializeSmartPost();
        cityList.setItems(greatLeader.getCityList());
    }

    @FXML
    private void addPackageAction(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLPackageAddition.fxml"));

        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setResizable(false);
        stage.initStyle(StageStyle.UTILITY);
        stage.setAlwaysOnTop(true);
        stage.show();
    }

    @FXML
    private void showAboutInfo(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLAboutInfoWindow.fxml"));

        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setResizable(false);
        stage.initStyle(StageStyle.UTILITY);
        stage.setAlwaysOnTop(true);
        stage.show();
    }

    @FXML
    private void resetItemListAction(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLRemovePackage.fxml"));

        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setResizable(false);
        stage.initStyle(StageStyle.UTILITY);
        stage.show();
    }


    @FXML
    private void updateStartSmartPostAction(ActionEvent event) {
        startSmartPost.setItems(greatLeader.getSmartPostList(startCityList.getValue()));
    }

    @FXML
    private void updateEndSmartPostAction(ActionEvent event) {
        endSmartPost.setItems(greatLeader.getSmartPostList(endCityList.getValue()));
    }

    @FXML
    private void openPackageListAction(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLPackageList.fxml"));

        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setResizable(false);
        stage.initStyle(StageStyle.UTILITY);
        stage.setAlwaysOnTop(true);
        stage.show();

    }

    @FXML
    private void updatePackagesAction(ActionEvent event) {
        packetList.setItems(greatLeader.getPackageList());
    }

    @FXML
    private void sendPackageAction(ActionEvent event) {
        SmartPost startL = startSmartPost.getValue();
        SmartPost endL = endSmartPost.getValue();
        Package sentPackage = packetList.getValue();
        if (startL != null && endL != null && sentPackage != null) {
            String pathArray = String.format("[%s, %s, %s, %s ]", startL.getLat(), startL.getLng(), endL.getLat(), endL.getLng());
            String scriptCall = String.format("document.getDistance(%s)", pathArray);
            System.out.println(scriptCall);
            int pathDistance = Math.round(Float.parseFloat(mapBox.getEngine().executeScript(scriptCall).toString()));
            System.out.println(pathDistance);
            if (sentPackage.getPackageClass().getDistance() == 0 || pathDistance < sentPackage.getPackageClass().getDistance()) {
                System.out.println(startL.getAddress());
                scriptCall = String.format("document.goToLocation('%s, %s, %s', '%s %s', 'red')", startL.getAddress(), 
                        startL.getPostalCode(), startL.getCity(), startL.getName(), startL.getAvailability());
                mapBox.getEngine().executeScript(scriptCall);
                scriptCall = String.format("document.goToLocation('%s, %s, %s', '%s %s', 'red')", endL.getAddress(), 
                        endL.getPostalCode(), endL.getCity(), endL.getName(), endL.getAvailability());
                mapBox.getEngine().executeScript(scriptCall);
                scriptCall = String.format("document.createPath(%s, %s, %d)", pathArray, "'red'", sentPackage.getPackageClass().getID());
                mapBox.getEngine().executeScript(scriptCall);
                greatLeader.sendPackage(sentPackage.getPackageID(), startL.getSmartPostID(), endL.getSmartPostID(), pathDistance);
                packetList.setItems(greatLeader.getPackageList());
                System.out.println(sentPackage.getContents().getFragile());
                System.out.println(sentPackage.getPackageClass().isWillBreak());
                if (sentPackage.getContents().getFragile() && sentPackage.getPackageClass().isWillBreak()) {
                    sendStatusLabel.setText("Lähetys lähetetty, mutta sen sisältö hajosi matkalla!");
                } else {
                    sendStatusLabel.setText("Lähetys lähetetty, mutta TIMOTEI miehet eivät onnistuneet rikkomaan sisältöä!");
                }
            } else {
                sendStatusLabel.setText("Lähetys hylätty, etäisyyttä liikaa");
            }
        } else {
            sendStatusLabel.setText("Invalid Selection");
        }
        
    }
    
    /**
     * deletes the database and creates it again and reloads the mapview,
     * removing map markers and lines *
     */
    @FXML
    private void nukeDatabaseAction(ActionEvent event) throws Exception {
        greatLeader.TrumpWontLikeThis(true);
        greatLeader.initializeSmartPost();
        cityList.setItems(greatLeader.getCityList());
        mapBox.getEngine().reload();
    }

    @FXML
    private void removePathsAction(ActionEvent event) {
        String scriptCall = String.format("document.deletePaths()");
        mapBox.getEngine().executeScript(scriptCall);
    }

}
