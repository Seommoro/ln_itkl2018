/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itkl_harkkatyö;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Lassi Nieminen IT-Kesäleiri 2018 Olio-ohjelmointia
 */
/**
 * DataHandler class works as medium between the main class and the methods of
 * controlling data *
 */
public class DataHandler {

    private final XMLHandler xmlHandler;
    private final DataBaseHandler dbHandler;
    private static DataHandler instance = null;

    private DataHandler() {
        xmlHandler = new XMLHandler();
        dbHandler = new DataBaseHandler();
    }

    public static DataHandler getInstance() {
        if (instance == null) {
            instance = new DataHandler();
        }
        return instance;
    }

    public void initializeSmartPostTable() {
        dbHandler.resetSmartPostTable();
        dbHandler.addSmartPostsToDatabase(xmlHandler.getSmartPostXML());
    }

    public ArrayList getCityNameList() {
        return dbHandler.getCityNameList();
    }

    public ArrayList getMapMarkerLocations(String cityName) {
        return dbHandler.getMapMarkerLocations(cityName);
    }

    public ArrayList getPackageClassList() {
        return dbHandler.getPackageClassListdb();
    }

    public ArrayList getItemList() {
        return dbHandler.getItemListdb();
    }

    public void addNewItemtoDatabase(int X, int Y, int Z, String name, int weight, boolean fragility) {
        dbHandler.addNewItemtoDatabase(X, Y, Z, name, weight, fragility);
    }

    public void addNewPackagetoDatabase(int itemID, int classID) {
        dbHandler.addNewPackagetoDatabase(itemID, classID);
    }

    public void removePackage(int packageID) {
        dbHandler.removePackagedb(packageID);
    }

    public ArrayList getSmartPostList(String cityName) {
        return dbHandler.getSmartPostList(cityName);
    }

    public ArrayList getPackageList() {
        return dbHandler.getPackageList();
    }
    public ArrayList getShippingList() {
        return dbHandler.getShippingList();
    }
    
    void sendPackage(int packageID, int startID, int endID, int pathDistance) {
        dbHandler.sendPackage(packageID, startID, endID, pathDistance);
    }

    public void FireAndFury(boolean isReset) {
        if (isReset) {
            dbHandler.Communism();
        }
        dbHandler.Capitalism();
    }


    /**
     * XMLHandler class contains the methods to connect and receive XML coded
     * information *
     */
    class XMLHandler {

        private Document doc;

        private XMLHandler() {

        }

        private String getXMLTotal() {
            String inputLine, total = "";
            try {
                URL smartPostUrl = new URL("http://iseteenindus.smartpost.ee/api/?request=destinations&country=FI&type=APT");
                BufferedReader in = new BufferedReader(new InputStreamReader(smartPostUrl.openStream(), "UTF-8"));
                while ((inputLine = in.readLine()) != null) {
                    total += inputLine;
                }
                return total;
            } catch (IOException e) {
                System.out.println("Something went wrong, XMLHandler:getXMLTotal()");
                return null;
            }
        }

        private void getDoc() {

            try {
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                doc = dBuilder.parse(new InputSource(new StringReader(getXMLTotal())));
                doc.getDocumentElement().normalize();
            } catch (ParserConfigurationException e) {
                System.err.println("Caught ParserConfigurationException!");
            } catch (IOException e) {
                System.err.println("Caught IOException!");
            } catch (SAXException e) {
                System.err.println("Caught SAXException!");
            }
        }

        public ArrayList<SmartPost> getSmartPostXML() {
            getDoc();
            ArrayList<SmartPost> SmartPostList = new ArrayList();
            NodeList nodes = doc.getElementsByTagName("item");
            for (int i = 0; i < nodes.getLength(); i++) {
                Node node = nodes.item(i);
                Element e = (Element) node;
                String name = e.getElementsByTagName("name").item(0).getTextContent();
                String city = e.getElementsByTagName("city").item(0).getTextContent();
                String address = e.getElementsByTagName("address").item(0).getTextContent();
                String postalcode = e.getElementsByTagName("postalcode").item(0).getTextContent();
                String availability = e.getElementsByTagName("availability").item(0).getTextContent();
                String lat = e.getElementsByTagName("lat").item(0).getTextContent();
                String lng = e.getElementsByTagName("lng").item(0).getTextContent();
                SmartPostList.add(new SmartPost(0, name, city, address, postalcode, availability, lat, lng));
            }
            return SmartPostList;
        }
    }

    /**
     * DataBaseHandler includes all the methods of accessing the database for
     * information *
     */
    class DataBaseHandler {

        private Connection connect() throws SQLException {
            Connection conn = null;
            try {
                String dbUrl = "jdbc:sqlite:HT.db";
                conn = DriverManager.getConnection(dbUrl);
                System.out.println("Connected.");
            } catch (SQLException e) {
                System.out.println("SQL failure on connecting, DataBaseHandler:connect()");
            } catch (Exception ex) {
                System.out.println("Something went wrong, DataBaseHandler:connect()");
            }
            conn.createStatement().execute("PRAGMA foreign_keys=ON");
            return conn;
        }

        /**
         * Gets list of SmartPosts from xmlHandler, adds the list to database*
         */
        public void addSmartPostsToDatabase(ArrayList<SmartPost> smartPostList) {
            System.out.println("Preparing to connect to database");
            String sqlCommand;
            SmartPost c;
            try (Connection conn = connect()) {
                System.out.println("Connected to database, adding SmartPosts");
                conn.setAutoCommit(false);
                for (int i = 0; i < smartPostList.size(); i++) {
                    c = smartPostList.get(i);
                    sqlCommand = String.format("INSERT OR IGNORE INTO "
                            + "KaupunkiPostinumero VALUES (%d, '%s')",
                            Integer.parseInt(c.getPostalCode()),
                            c.getCity());
                    conn.prepareStatement(sqlCommand).executeUpdate();
                    sqlCommand = String.format("INSERT OR IGNORE INTO SmartPost VALUES "
                            + "(%d, '%s',%d ,'%s','%s','%s','%s')", (i + 1),
                            c.getName(), Integer.parseInt(c.getPostalCode()),
                            c.getAddress(), c.getAvailability(),
                            c.getLat(), c.getLng());
                    conn.prepareStatement(sqlCommand).executeUpdate();
                }
                conn.commit();
                System.out.println("SmartPosts added to database");
                conn.setAutoCommit(true);
            } catch (SQLException e) {
                System.out.println(e);
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }

        public void resetSmartPostTable() {
            String sqlCommand;
            System.out.println("Preparing to connect to database to reset SmartPost table");
            try (Connection conn = connect()) {
                System.out.println("Connected to database, resetting smartposts");
                sqlCommand = "DELETE FROM smartpost";
                conn.prepareStatement(sqlCommand).executeUpdate();
                System.out.println("Reset done");
            } catch (SQLException e) {
                System.out.println(e);
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }

        public ArrayList getCityNameList() {
            ResultSet results;
            ArrayList<String> cityNameList = new ArrayList();
            try (Connection conn = connect()) {
                String sqlCommand = "SELECT DISTINCT kaupunki FROM kaupunkipostinumero ORDER BY kaupunki";
                results = conn.createStatement().executeQuery(sqlCommand);

                while (results.next()) {
                    cityNameList.add(results.getString(1));
                }

                return cityNameList;
            } catch (SQLException sqle) {
                System.out.println(sqle);
            } catch (Exception e) {
                System.out.println(e);
            }
            return null;
        }

        public ArrayList getMapMarkerLocations(String cityName) {
            ResultSet results;
            ArrayList<String> mapMarkerCommands = new ArrayList();
            try (Connection conn = connect()) {
                String sqlCommand = String.format("SELECT osoite, postinumero, "
                        + "kaupunki, nimi, aukioloajat FROM KokoSP "
                        + "WHERE kaupunki = '%s'", cityName);
                results = conn.createStatement().executeQuery(sqlCommand);

                while (results.next()) {
                    mapMarkerCommands.add(String.format("'%s, %s, %s', '%s %s'",
                            results.getString(1), results.getString(2),
                            results.getString(3), results.getString(4),
                            results.getString(5)));
                }

                System.out.println(mapMarkerCommands);
                return mapMarkerCommands;
            } catch (SQLException sqle) {
                System.out.println(sqle);
            } catch (Exception e) {
                System.out.println(e);
            }
            return null;
        }

        public ArrayList getPackageClassListdb() {
            ResultSet results;
            ArrayList<PackageClass> packageClassList = new ArrayList();
            try (Connection conn = connect()) {
                String sqlCommand = "SELECT luokkaID, maxx, maxy, maxz, maxpaino, maxmatka, rikkoutuminen FROM luokka";
                results = conn.createStatement().executeQuery(sqlCommand);

                while (results.next()) {
                    PackageClass newClass = new PackageClass(results.getInt(1), results.getInt(2),
                            results.getInt(3), results.getInt(4), results.getInt(5), results.getInt(6),
                            results.getBoolean(7));
                    packageClassList.add(newClass);
                }

                return packageClassList;
            } catch (SQLException sqle) {
                System.out.println(sqle);
            } catch (Exception e) {
                System.out.println(e);
            }
            return null;
        }

        public ArrayList getItemListdb() {
            ResultSet results;
            ArrayList<Item> itemList = new ArrayList();
            try (Connection conn = connect()) {
                String sqlCommand = "SELECT esineID, nimi, x, y, z, paino, hauras FROM esine";
                results = conn.createStatement().executeQuery(sqlCommand);

                while (results.next()) {
                    Item newItem = new Item(results.getString(2), results.getInt(1),
                            results.getInt(3), results.getInt(4), results.getInt(5),
                            results.getInt(6), results.getBoolean(7));
                    itemList.add(newItem);
                }

                return itemList;
            } catch (SQLException sqle) {
                System.out.println(sqle);
            } catch (Exception e) {
                System.out.println(e);
            }
            return null;
        }

        /**
         *
         * @param X
         * @param Y
         * @param Z
         * @param name
         * @param weight
         * @param fragility
         */
        public void addNewItemtoDatabase(int X, int Y, int Z, String name, int weight, boolean fragility) {
            String sqlCommand;
            try (Connection conn = connect()) {
                sqlCommand = String.format("INSERT OR IGNORE INTO "
                        + "Esine VALUES (null, '%s', %d, %b, %d, %d, %d)",
                        name, weight, fragility, X, Y, Z);
                conn.prepareStatement(sqlCommand).executeUpdate();
            } catch (SQLException e) {
                System.out.println(e);
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }

        public void addNewPackagetoDatabase(int itemID, int classID) {
            String sqlCommand;
            try (Connection conn = connect()) {
                sqlCommand = String.format("INSERT INTO "
                        + "Paketti VALUES (null, %d, %d)",
                        classID, itemID);
                System.out.println("Adding package to packagetable");
                conn.prepareStatement(sqlCommand).executeUpdate();
                sqlCommand = "SELECT last_insert_rowid()";
                ResultSet results = conn.createStatement().executeQuery(sqlCommand);
                results.next();
                int lastRowID = results.getInt(1);
                System.out.println("Adding to storage");
                sqlCommand = String.format("INSERT INTO Varastointi"
                        + " VALUES (%d, 1)", lastRowID);
                conn.prepareStatement(sqlCommand).executeUpdate();
            } catch (SQLException e) {
                System.out.println(e);
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }

        private void removePackagedb(int packageID) {
            String sqlCommand;
            try (Connection conn = connect()) {
                sqlCommand = String.format("DELETE FROM paketti "
                        + "WHERE pakettiID = %d", packageID);
                conn.prepareStatement(sqlCommand).executeUpdate();
            } catch (SQLException e) {
                System.out.println(e);
            } catch (Exception ex) {
                System.out.println(ex);
            }

        }

        private ArrayList getSmartPostList(String cityName) {
            ResultSet results;
            ArrayList<SmartPost> smartPostList = new ArrayList();
            try (Connection conn = connect()) {
                String sqlCommand = "SELECT nimi, Kaupunki, Osoite, Postinumero, Aukioloajat, lat, lng, SmartPostID FROM KokoSP WHERE kaupunki = '" + cityName + "'";
                results = conn.createStatement().executeQuery(sqlCommand);

                while (results.next()) {
                    SmartPost newSmartPost = new SmartPost(results.getInt(8), results.getString(1), results.getString(2), results.getString(3), results.getString(4),
                            results.getString(5), results.getString(6), results.getString(7));
                    smartPostList.add(newSmartPost);
                }

                return smartPostList;
            } catch (SQLException sqle) {
                System.out.println(sqle);
            } catch (Exception e) {
                System.out.println(e);
            }
            return null;
        }

        private ArrayList getPackageList() {

            ResultSet results;
            ArrayList<Package> packageList = new ArrayList();
            try (Connection conn = connect()) {
                String sqlCommand = "SELECT paketti.pakettiID, Luokka.LuokkaID, MaxMatka, Rikkoutuminen, Esine.EsineID, Nimi, Hauras "
                        + "FROM paketti INNER JOIN luokka ON paketti.LuokkaID = luokka.LuokkaID "
                        + "INNER JOIN esine ON paketti.EsineID = esine.EsineID INNER JOIN varastointi ON paketti.pakettiID = varastointi.pakettiID";
                results = conn.createStatement().executeQuery(sqlCommand);

                while (results.next()) {
                    System.out.println(results.getString(4));
                    Package newPackage = new Package(results.getInt(1), new Item(results.getString(6), results.getInt(5), results.getString(7).equals("1")),
                            new PackageClass(results.getInt(2), results.getInt(3), results.getString(4).equals("1")));
                    packageList.add(newPackage);
                }

                return packageList;
            } catch (SQLException sqle) {
                System.out.println(sqle);
            } catch (Exception e) {
                System.out.println(e);
            }
            return null;

        }
        
        private ArrayList getShippingList() {

            ResultSet results;
            ArrayList<Shipping> shippingList = new ArrayList();
            try (Connection conn = connect()) {
                String sqlCommand = "SELECT paketti.pakettiID, Luokka.LuokkaID, MaxMatka, Rikkoutuminen, Esine.EsineID, esine.Nimi, Hauras, kuljetusID, sa.nimi, ba.nimi "
                        + "FROM paketti INNER JOIN luokka ON paketti.LuokkaID = luokka.LuokkaID "
                        + "INNER JOIN esine ON paketti.EsineID = esine.EsineID INNER JOIN kuljetukset ON paketti.pakettiID = kuljetukset.pakettiID "
                        + "INNER JOIN SmartPost sa ON sa.smartpostID = kuljetukset.alku "
                        + "INNER JOIN SmartPost ba ON ba.smartpostID = kuljetukset.loppu";
                results = conn.createStatement().executeQuery(sqlCommand);

                while (results.next()) {
                    System.out.println(results.getString(4));
                    Package newPackage = new Package(results.getInt(1), new Item(results.getString(6), results.getInt(5), results.getString(7).equals("1")),
                            new PackageClass(results.getInt(2), results.getInt(3), results.getString(4).equals("1")));
                    Shipping newShipping = new Shipping(results.getInt(8), newPackage, new SmartPost(results.getString(9)), new SmartPost(results.getString(10)));
                    
                    shippingList.add(newShipping);
                }

                return shippingList;
            } catch (SQLException sqle) {
                System.out.println(sqle);
            } catch (Exception e) {
                System.out.println(e);
            }
            return null;

        }
        
        private void sendPackage(int packageID, int startID, int endID, int pathDistance) {
            String sqlCommand;
            try (Connection conn = connect()) {
                sqlCommand = String.format("DELETE FROM varastointi "
                        + "WHERE pakettiID = %d;",
                        packageID);
                conn.createStatement().executeUpdate(sqlCommand);
                sqlCommand = String.format("INSERT INTO Kuljetukset VALUES (null, %d, %d, %d, "
                        + "(datetime('now','localtime')), (datetime('now','localtime')), %d);", packageID, startID, endID, pathDistance);
                conn.createStatement().executeUpdate(sqlCommand);
            } catch (SQLException e) {
                System.out.println(e);
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
        
            

        /**
         * Communism doesn't work so it only deletes the database
         */
        private void Communism() {
            try {
                File f = new File("HT.db");
                f.delete();
            } catch (Exception e) {
                System.out.println(e);
            }

        }

        /**
         * Capitalism fixes the commie mess and rebuilds the database
         */
        private void Capitalism() {
            String sqlCommand;
            try (Connection conn = connect()) {
                sqlCommand = "";
                String readLine;
                BufferedReader sqlReader = new BufferedReader(new InputStreamReader(
                        this.getClass()
                                .getResourceAsStream("/Othersource/DatabaseFile/HTDB.sql")));
                while ((readLine = sqlReader.readLine()) != null) {
                    sqlCommand = sqlCommand + readLine + "\n";
                }
                System.out.println(sqlCommand);
                conn.createStatement().executeUpdate(sqlCommand);
                System.out.println("did it work?");
                sqlReader.close();
            } catch (SQLException e) {
                System.out.println(e);
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }


    }
}
