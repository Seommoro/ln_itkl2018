/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package itkl_harkkatyö;

/**
 *
 * @author Lassi Nieminen
 *          IT-Kesäleiri 2018 Olio-ohjelmointia
 */
public class Item {
    private String name;
    private int ID;
    private int xSize;
    private int ySize;
    private int zSize;
    private int weight;
    private boolean fragility;
    
    public Item(String naame, int iidee, int x, int y, int z, int w, boolean f) {
        name = naame;
        ID = iidee;
        xSize = x;
        ySize = y;
        zSize = z;
        weight = w;
        fragility = f;
    }
    public Item(String naame, int iidee, boolean f) {
        name = naame;
        ID = iidee;
        xSize = 0;
        ySize = 0;
        zSize = 0;
        weight = 0;
        fragility = f;
    }
    
    
    public String getName(){
        return name;
    }
    
    public int getID(){
        return ID;
    }
    
    public int getX(){
        return xSize;
    }
    
    public int getY(){
        return ySize;
    }
    
    public int getZ(){
        return zSize;
    }
    
    public int getWeight(){
        return weight;
    }
    
    public boolean getFragile(){
        return fragility;
    }
    
    
    @Override
    public String toString() {
        return name;
    }
}
