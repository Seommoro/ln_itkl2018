/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package itkl_harkkatyö;

/**
 *
 * @author Lassi Nieminen
 *          IT-Kesäleiri 2018 Olio-ohjelmointia
 */
public class Package {
    private int packageID;
    private Item contents;
    private PackageClass packageClass;
    
    
    
    public Package(int newID, Item newContent, PackageClass newPackageClass){
        packageID = newID;
        contents = newContent;
        packageClass = newPackageClass;
    }
    


    public int getPackageID() {
        return packageID;
    }

    public Item getContents() {
        return contents;
    }

    public PackageClass getPackageClass() {
        return packageClass;
    }
    
    

    @Override
    public String toString() {
        return "PackageID: " + packageID + ", Contents: " + contents.getName() + ", Package Class: " + packageClass.getID();
    }
    
}
