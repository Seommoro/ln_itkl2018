/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package itkl_harkkatyö;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Lassi Nieminen
 *          IT-Kesäleiri 2018 Olio-ohjelmointia
 */
public class GreatLeader {
        
    DataHandler dataHandler;
    private static GreatLeader instance = null;
    
    private GreatLeader(){
        dataHandler = DataHandler.getInstance();
    }
    
    public static GreatLeader getInstance(){
        if (instance == null) {
            instance = new GreatLeader();
        }
        return instance;
    }
    
    public void initializeSmartPost(){
        dataHandler.initializeSmartPostTable();
    }
    
    
    public ObservableList getCityList(){
        ObservableList<String> cityList = FXCollections.observableArrayList(dataHandler.getCityNameList());
        return cityList;
    }
    
    
    /** Takes string as parameter and gives it to DataHandler, gets ArrayList from DataHandler and returns the arraylist as normal array**/
    public String[] getMapMarkerCommands(String cityName) {
        ArrayList<String> commands = dataHandler.getMapMarkerLocations(cityName);;
        String[] commandArray = new String[ commands.size()];
        commands.toArray(commandArray);
        return commandArray;
    }
    
    public ObservableList getPackageClassList() {
        ObservableList<PackageClass> classList = FXCollections.observableArrayList(dataHandler.getPackageClassList());
        return classList;
    }
    
    public ObservableList getItemList() {
        ObservableList<Item> itemList = FXCollections.observableArrayList(dataHandler.getItemList());
        return itemList;
    }
    
    public void addNewItemToDatabase(int X, int Y, int Z, String name, int weight, boolean fragility) {
        dataHandler.addNewItemtoDatabase(X, Y, Z, name, weight, fragility);
    }
    
    public void addNewPackageToDatabase(int itemID, int classID) {
        dataHandler.addNewPackagetoDatabase(itemID, classID);
    }
    
    public void removePackage(int packageID){
        dataHandler.removePackage(packageID);
    }
    
    ObservableList<SmartPost> getSmartPostList(String cityName) {
        ObservableList<SmartPost> smartPostList = FXCollections.observableArrayList(dataHandler.getSmartPostList(cityName));
        return smartPostList;
    }
    
    ObservableList<Package> getPackageList() {
        ObservableList<Package> packageList = FXCollections.observableArrayList(dataHandler.getPackageList());
        return packageList;
    }
    ObservableList<Shipping> getShippingList() {
        ObservableList<Shipping> shippingList = FXCollections.observableArrayList(dataHandler.getShippingList());
        return shippingList;
    }
    
    void sendPackage(int packageID, int startID, int endID, int pathDistance) {
        dataHandler.sendPackage(packageID, startID, endID, pathDistance);
    }
    
    /** Requests for required methods to reset the database completely **/
    public void TrumpWontLikeThis(boolean isReset){
        dataHandler.FireAndFury(isReset);
    }




}
