PRAGMA foreign_keys=OFF;
CREATE TABLE IF NOT EXISTS "KaupunkiPostinumero" (
	"Postinumero" INTEGER PRIMARY KEY,
	"Kaupunki" VARCHAR(32) NOT NULL
);

CREATE TABLE IF NOT EXISTS "SmartPost" (
	"SmartPostID" INTEGER PRIMARY KEY,
	"Nimi" VARCHAR(32) NOT NULL,
	"Postinumero" INTEGER NOT NULL,
	"Osoite" VARCHAR(32) NOT NULL,
	"Aukioloajat" VARCHAR(48) NOT NULL,
	"lat" VARCHAR(10) NOT NULL,
	"lng" VARCHAR(10) NOT NULL,

	FOREIGN KEY("Postinumero") REFERENCES "KaupunkiPostinumero"("Postinumero") ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS "Varasto" (
	"VarastoID" INTEGER PRIMARY KEY,
	"Nimi" VARCHAR(20) NOT NULL
);

INSERT OR IGNORE INTO Varasto VALUES (1, 'Neljäs ulottuvuus');

CREATE TABLE IF NOT EXISTS "Luokka" (
	"LuokkaID" INTEGER PRIMARY KEY,
	"MaxX" INTEGER NOT NULL CHECK ("MaxX" >= 0),
	"MaxY" INTEGER NOT NULL CHECK ("MaxY" >= 0),
	"MaxZ" INTEGER NOT NULL CHECK ("MaxZ" >= 0),
	"MaxMatka" INTEGER NOT NULL CHECK ("MaxMatka" >= 0),
	"MaxPaino" INTEGER NOT NULL CHECK ("MaxPaino" >= 0),
	"Rikkoutuminen" BOOLEAN NOT NULL
);

INSERT OR IGNORE INTO Luokka VALUES (1, 50, 50, 50, 150, 100, true);
INSERT OR IGNORE INTO Luokka VALUES (2, 20, 20, 20, 0, 100, false);
INSERT OR IGNORE INTO Luokka VALUES (3, 50, 50, 50, 0, 10000, true);

CREATE TABLE IF NOT EXISTS "Esine" (
	"EsineID" INTEGER PRIMARY KEY,
	"Nimi" VARCHAR(32) NOT NULL,
	"Paino" INTEGER NOT NULL CHECK ("Paino" >= 0),
	"Hauras" BOOLEAN NOT NULL,
	"X" INTEGER NOT NULL CHECK ("X" >= 0),
	"Y" INTEGER NOT NULL CHECK ("Y" >= 0),
	"Z" INTEGER NOT NULL CHECK ("Z" >= 0)
);


INSERT INTO Esine VALUES (null, "Example1", 10, true, 10, 10, 10);
INSERT INTO Esine VALUES (null, "Example2", 200, true, 10, 10, 10);
INSERT INTO Esine VALUES (null, "Example3", 200, true, 30, 30, 30);

CREATE TABLE IF NOT EXISTS "Paketti" (
	"PakettiID" INTEGER PRIMARY KEY,
	"LuokkaID" INTEGER NOT NULL,
	"EsineID" INTEGER NOT NULL,

	CONSTRAINT pakettiLuokka FOREIGN KEY ("LuokkaID") REFERENCES "Luokka"("LuokkaID") ON DELETE CASCADE,
	CONSTRAINT pakettiEsine FOREIGN KEY ("EsineID") REFERENCES "Esine"("EsineID") ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS "Varastointi" (
	"PakettiID" INTEGER NOT NULL,
	"VarastoID" INTEGER NOT NULL,

	
	FOREIGN KEY ("PakettiID") REFERENCES "Paketti"("PakettiID") ON DELETE CASCADE,
	FOREIGN KEY ("VarastoID") REFERENCES "Varasto"("VarastoID") ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS "Kuljetukset" (
	"KuljetusID" INTEGER PRIMARY KEY,
	"PakettiID" INTEGER NOT NULL,
	"Alku" INTEGER NOT NULL,
	"Loppu" INTEGER NOT NULL,
	"Alkuaika" TIMESTAMP NOT NULL,
	"Loppuaika" TIMESTAMP NOT NULL,
	"Matka" INTEGER NOT NULL,

	FOREIGN KEY ("PakettiID") REFERENCES "Paketti"("PakettiID") ON DELETE CASCADE,
	FOREIGN KEY ("Alku") REFERENCES "SmartPost"("SmartPostID"),
	FOREIGN KEY ("Loppu") REFERENCES "SmartPost"("SmartPostID")
);

CREATE VIEW IF NOT EXISTS "KokoSP" AS 
SELECT "SmartPostID",
	"Nimi",
	"SmartPost"."Postinumero",	
	"Kaupunki",
	"Osoite",
	"Aukioloajat",
	"lat",
	"lng" FROM "SmartPost" 
	INNER JOIN "KaupunkiPostinumero" 
	ON SmartPost.Postinumero = KaupunkiPostinumero.Postinumero;

	PRAGMA foreign_keys=ON;