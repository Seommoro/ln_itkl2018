/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package viikko9;

/**
 *
 * @author Lassi Nieminen
 *          IT-Kesäleiri 2018 Olio-ohjelmointia
 */
public class Elokuva {
    
    protected String Nimi;
    protected String alkuAika;
    protected String esitysPaikka;
    
    public Elokuva(String nimi, String alku, String esityspaikka) {
        alkuAika = alku;
        Nimi = nimi;
        esitysPaikka = esityspaikka;
    }
    
    public Elokuva() {
        
    }
    
    public String getAlkuAika(){
        return alkuAika;
    }
    
    @Override
    public String toString(){
        
        return alkuAika+"\t"+Nimi;
        
    }
    
    public String getName(){
        return Nimi;
    }

}

class TeatteriElokuva extends Elokuva {
    
    
    public TeatteriElokuva(String nimi, String alku, String esityspaikka) {
        alkuAika = alku;
        Nimi = nimi;
        esitysPaikka = esityspaikka;
    }
    
    @Override
    public String toString(){
        return alkuAika + "\t" + esitysPaikka;
    }
}