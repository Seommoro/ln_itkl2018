/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package viikko9;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Lassi Nieminen
 *          IT-Kesäleiri 2018 Olio-ohjelmointia
 */
public class Elokuvalista {
    
    public Elokuvalista()
    {    }
    
    public ObservableList getMovies(String date, String area, int Alku, int Loppu) {
        String inputLine, total = "";
        try {
            URL teatteriUrl = new URL("https://www.finnkino.fi/xml/Schedule/?area="+area+"&dt="+date);
            BufferedReader in = new BufferedReader(new InputStreamReader(teatteriUrl.openStream(), "UTF-8"));
            while ((inputLine = in.readLine()) != null) {
                total += inputLine;
            }

        } catch (Exception e) {
            System.out.println("fug");
        }
        TeatteriXML teatteri = new TeatteriXML(total);
        ArrayList m = teatteri.getMovieMap(Alku, Loppu);
        ObservableList derp = FXCollections.observableList(m);
        return derp;
    }
    
    public ObservableList getMovies(String date, int Alku, int Loppu) {
        String inputLine, total = "";
        try {
            URL teatteriUrl = new URL("https://www.finnkino.fi/xml/Schedule/?dt="+date);
            BufferedReader in = new BufferedReader(new InputStreamReader(teatteriUrl.openStream(), "UTF-8"));
            while ((inputLine = in.readLine()) != null) {
                total += inputLine;
            }

        } catch (Exception e) {
            System.out.println("fug");
        }
        TeatteriXML teatteri = new TeatteriXML(total);
        ArrayList m = teatteri.getMovieMap(Alku, Loppu);
        ObservableList derp = FXCollections.observableList(m);
        return derp;
    }
    public ObservableList getMovies(String date, String area) {
        return getMovies (date, area, 0, 2400);
    }
    public ObservableList getMovies(String date) {
        return getMovies(date, 0, 2400);
    }
    
    public ObservableList getTheatresByMovie(String date, String area, String hakusanat) {
        String inputLine, total = "";
        try {
            URL teatteriUrl = new URL("https://www.finnkino.fi/xml/Schedule/?area="+area+"&dt="+date);
            BufferedReader in = new BufferedReader(new InputStreamReader(teatteriUrl.openStream(), "UTF-8"));
            while ((inputLine = in.readLine()) != null) {
                total += inputLine;
            }

        } catch (Exception e) {
            System.out.println("fug");
        }
        TeatteriXML teatteri = new TeatteriXML(total);
        ArrayList m = teatteri.getSearchMap(hakusanat);
        ObservableList derp = FXCollections.observableList(m);
        return derp;
    }
}
