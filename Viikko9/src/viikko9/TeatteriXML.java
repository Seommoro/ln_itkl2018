/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko9;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Lassi Nieminen IT-Kesäleiri 2018 Olio-ohjelmointia
 */
public class TeatteriXML {

    private Document doc;
    private ArrayList m;
    private ArrayList k;
    private ArrayList searchResults;

    public TeatteriXML(String total) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(new InputSource(new StringReader(total)));
            doc.getDocumentElement().normalize();
        } catch (ParserConfigurationException e) {
            System.err.println("Caught ParserConfigurationException!");
        } catch (IOException e) {
            System.err.println("Caught IOException!");
        } catch (SAXException e) {
            System.err.println("Caught SAXException!");
        }
    }

    public ArrayList getTheatreMap() {
        m = new ArrayList();
        getTheatreData();
        return m;
    }
    public ArrayList getMovieMap(int Alku, int Loppu) {
        k = new ArrayList();
        getMovieData(Alku, Loppu);
        return k;
    }
    
    public ArrayList getSearchMap(String hakusanat) {
        searchResults = new ArrayList();
        getSearchResults(hakusanat);
        return searchResults;
    }

    protected void getTheatreData() {
        NodeList nodes = doc.getElementsByTagName("TheatreArea");
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            Teatteri uusiTeatteri = 
            new Teatteri(e.getElementsByTagName("ID").item(0).getTextContent(), e.getElementsByTagName("Name").item(0).getTextContent());
            m.add(uusiTeatteri);
        }
    }
    
    protected void getMovieData(int Alku, int Loppu) {
        NodeList nodes = doc.getElementsByTagName("Show");
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            String kello = e.getElementsByTagName("dttmShowStart").item(0).getTextContent().split("T")[1];
            String[] kello1 = kello.split(":");
            int kello2 = Integer.parseInt(kello1[0]+kello1[1]);
            if (kello2 >=Alku && kello2 <= Loppu){
                System.out.println(Alku);
                System.out.println(kello2);
                System.out.println(Loppu);
                Elokuva uusiElokuva = 
                        new Elokuva(e.getElementsByTagName("Title").item(0).getTextContent(), kello1[0]+":"+kello1[1],e.getElementsByTagName("TheatreAndAuditorium").item(0).getTextContent());
                k.add(uusiElokuva);
            }
            
        }
   }
    protected void getSearchResults(String hakusanat) {
        NodeList nodes = doc.getElementsByTagName("Show");
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            String kello = e.getElementsByTagName("dttmShowStart").item(0).getTextContent().split("T")[1];
            String[] kello1 = kello.split(":");
            String elokuvaNimi = e.getElementsByTagName("Title").item(0).getTextContent().toLowerCase();
            if (elokuvaNimi.contains(hakusanat.toLowerCase())) {
                TeatteriElokuva uusiTulos = 
                        new TeatteriElokuva(e.getElementsByTagName("Title").item(0).getTextContent(), kello1[0]+":"+kello1[1],e.getElementsByTagName("TheatreAndAuditorium").item(0).getTextContent());
            searchResults.add(uusiTulos);
            }
        }
   }

}
