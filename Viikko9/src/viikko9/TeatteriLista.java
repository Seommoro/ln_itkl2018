/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package viikko9;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Lassi Nieminen
 *          IT-Kesäleiri 2018 Olio-ohjelmointia
 */
public class TeatteriLista {
    
    private static TeatteriLista instance = null;
    
    private TeatteriLista(){    }
    
    public static TeatteriLista getInstance() {
        if (instance == null) {
            instance = new TeatteriLista();
            return instance;
        } else {
            return instance;
        }
    }
    public ObservableList getTheatres() {
        String inputLine, total = "";
        try {
            URL teatteriUrl = new URL("https://www.finnkino.fi/xml/TheatreAreas?mode=xml");
            BufferedReader in = new BufferedReader(new InputStreamReader(teatteriUrl.openStream(), "UTF-8"));
            while ((inputLine = in.readLine()) != null) {
                total += inputLine;
            }

        } catch (Exception e) {
            System.out.println("fug");
        }
        TeatteriXML teatteri = new TeatteriXML(total);
        ArrayList m = teatteri.getTheatreMap();
        ObservableList derp = FXCollections.observableList(m);
        return derp;

    }
    
}
