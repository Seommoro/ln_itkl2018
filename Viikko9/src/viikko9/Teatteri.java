/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package viikko9;

/**
 *
 * @author Lassi Nieminen
 *          IT-Kesäleiri 2018 Olio-ohjelmointia
 */
public class Teatteri {
    private String ID;
    private String Nimi;
    
    public Teatteri(String id, String nimi) {
        ID = id;
        Nimi = nimi;
    }
    
    public String getID(){
        return ID;
    }
    
    @Override
    public String toString(){
        return Nimi;
    }

}
