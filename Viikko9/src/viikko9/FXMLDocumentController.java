/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko9;

import com.sun.javafx.scene.traversal.Direction;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import java.util.Date;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.input.KeyEvent;

/**
 *
 * @author p9042
 */
public class FXMLDocumentController implements Initializable {

    //Map<String, Elokuvateatteri> teatteriLista = new HashMap();
    private Label label;
    @FXML
    private ComboBox<Teatteri> teatteriListaBox;
    @FXML
    private TextField alkuTunti;
    @FXML
    private TextField alkuMinuutti;
    @FXML
    private TextField loppuTunti;
    @FXML
    private TextField loppuMinuutti;
    @FXML
    private TextField nimiHakuField;
    @FXML
    private TextField dayList;
    @FXML
    private ListView<Integer> dayBigList;
    
    private int i = 1;
    private int o = 1;
    private int p = 1;
    
    @FXML
    private ListView<Integer> monthBigList;
    @FXML
    private TextField monthList;
    @FXML
    private ListView<?> yearBigList;
    @FXML
    private TextField yearList;
    @FXML
    private ListView resultList;
    @FXML
    private Button listMoviesButton;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        TeatteriLista teatteriInstance = TeatteriLista.getInstance();
        teatteriListaBox.setItems(teatteriInstance.getTheatres());
        teatteriListaBox.getSelectionModel().selectFirst();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	Date date = new Date();
        String[] dates = dateFormat.format(date).split("/");
        dayList.setText(dates[0].replaceFirst("^0(?!$)", ""));
        i = Integer.parseInt(dates[0]);
        monthList.setText(dates[1].replaceFirst("^0(?!$)", ""));
        o = Integer.parseInt(dates[1]);
        yearList.setText(dates[2]);
        p = Integer.parseInt(dates[2]);
        

    }

    @FXML
    private void dayBigListScrollAction(ScrollEvent event) {
        if (event.getDeltaY() > 0) {
            i += 1;
            if (i==32){i=1;}
        } else {
            i -= 1;
            if (i==0){i=31;}
        }
        
        listaDayKutsu();
    }

    private void listaDayKutsu() {
        
        List paivaList = new ArrayList();
        if (i == 1) {
            paivaList.add(i+1);
            paivaList.add(i);
            paivaList.add(31);
        } else if (i == 31) {
            paivaList.add(1);
            paivaList.add(i);
            paivaList.add(i-1);
        } else {
            paivaList.add(i+1);
            paivaList.add(i);
            paivaList.add(i-1);
            
        }

        dayBigList.getItems().clear();
        dayBigList.getItems().addAll(paivaList);

    }

    @FXML
    private void dayListClickAction(MouseEvent event) {
        listaDayKutsu();
        dayBigList.setDisable(false);
        dayBigList.setVisible(true);
        dayList.setVisible(false);
        dayList.setDisable(true);
    }

    @FXML
    private void CloseBigDayList(MouseEvent event) {
        dayBigList.setVisible(false);
        dayList.setDisable(false);
        dayList.setText(Integer.toString(i));
        dayList.setVisible(true);
        dayBigList.setDisable(true);
    }
    private void listaMonthKutsu() {
        
        List monthList = new ArrayList();
        if (o == 1) {
            monthList.add(o+1);
            monthList.add(o);
            monthList.add(12);
        } else if (o == 12) {
            monthList.add(1);
            monthList.add(o);
            monthList.add(o-1);
        } else {
            monthList.add(o+1);
            monthList.add(o);
            monthList.add(o-1);
            
        }

        monthBigList.getItems().clear();
        monthBigList.getItems().addAll(monthList);

    }
    
    @FXML
    private void monthListClickAction(MouseEvent event) {
        listaMonthKutsu();
        monthBigList.setDisable(false);
        monthBigList.setVisible(true);
        monthList.setVisible(false);
        monthList.setDisable(true);
    }
    @FXML
    private void monthBigListScrollAction(ScrollEvent event) {
        if (event.getDeltaY() > 0) {
            o += 1;
            if (o==13){o=1;}
        } else {
            o -= 1;
            if (o==0){o=12;}
        }
        
        listaMonthKutsu();
    }
    
    private void listaYearKutsu() {
        List yearList = new ArrayList();
        
            yearList.add(p+1);
            yearList.add(p);
            yearList.add(p-1);
            

        yearBigList.getItems().clear();
        yearBigList.getItems().addAll(yearList);
        
    }
    
    @FXML
    private void yearBigListScrollAction(ScrollEvent event) {
        if (event.getDeltaY() > 0) {
            p += 1;
        } else {
            p -= 1;
        }
        
        listaYearKutsu();
    }
    @FXML
    private void CloseBigYearList(MouseEvent event) {
        yearBigList.setVisible(false);
        yearList.setDisable(false);
        yearList.setText(Integer.toString(p));
        yearList.setVisible(true);
        yearBigList.setDisable(true);
    }

    @FXML
    private void yearListClickAction(MouseEvent event) {
        listaYearKutsu();
        yearBigList.setDisable(false);
        yearBigList.setVisible(true);
        yearList.setVisible(false);
        yearList.setDisable(true);
    }
    
    @FXML
    private void CloseBigMonthList(MouseEvent event) {
        monthBigList.setVisible(false);
        monthList.setDisable(false);
        monthList.setText(Integer.toString(o));
        monthList.setVisible(true);
        monthBigList.setDisable(true);
    }
    

    @FXML
    private void listMoviesAction(ActionEvent event) {
        Elokuvalista elokuvaLista = new Elokuvalista();
        
        String paiva = "";
        if (dayList.getText().length() == 1) {
            paiva = "0";
        }
        paiva += dayList.getText();
        
        String kuukausi = "";
        if (monthList.getText().length() == 1) {
            kuukausi = "0";
        }
        kuukausi += monthList.getText();
        
        String currentDay = paiva+"."+kuukausi+"."+yearList.getText();
        if (alkuTunti.getText().isEmpty() || alkuMinuutti.getText().isEmpty() || loppuTunti.getText().isEmpty() || loppuMinuutti.getText().isEmpty()) {
            if (teatteriListaBox.getValue().getID().equals("1029")) {
                resultList.setItems(elokuvaLista.getMovies(currentDay));
            } else {
                resultList.setItems(elokuvaLista.getMovies(currentDay, teatteriListaBox.getValue().getID()));
            }
        } else {
            int aloitusAika = Integer.parseInt(alkuTunti.getText()+alkuMinuutti.getText());
            int lopetusAika = Integer.parseInt(loppuTunti.getText()+ loppuMinuutti.getText());
            if (teatteriListaBox.getValue().getID().equals("1029")) {
                resultList.setItems(elokuvaLista.getMovies(currentDay, aloitusAika, lopetusAika));
            } else {
                resultList.setItems(elokuvaLista.getMovies(currentDay, teatteriListaBox.getValue().getID(), aloitusAika, lopetusAika));
            }
        }
        
        alkuTunti.clear();
        alkuMinuutti.clear();
        loppuTunti.clear();
        loppuMinuutti.clear();
    }

    @FXML
    private void timeWriteActionAlkuHour(KeyEvent event) {
        if(event.getSource()instanceof TextField){
            TextField muokattava =(TextField) event.getSource();
            if(muokattava.getText().length() > 2 || !isNumeric(muokattava.getText())) {
                muokattava.clear();
            }
            if (muokattava.getText().length() == 2){
                if(Integer.parseInt(muokattava.getText()) > 23) {
                    muokattava.setText("23");
                    alkuMinuutti.requestFocus();
                    alkuMinuutti.clear();
                } else {
                    alkuMinuutti.requestFocus();
                    alkuMinuutti.clear();
                }
            } 
        }
    }
    
    @FXML
    private void timeWriteActionAlkuMinute(KeyEvent event) {
        if(event.getSource()instanceof TextField){
            TextField muokattava =(TextField) event.getSource();
            if(muokattava.getText().length() > 2 || !isNumeric(muokattava.getText())) {
                muokattava.clear();
            }
            if (muokattava.getText().length() == 2){
                if(Integer.parseInt(muokattava.getText()) > 59) {
                    muokattava.setText("59");
                    loppuTunti.requestFocus();
                    loppuTunti.clear();
                } else {
                    loppuTunti.requestFocus();
                    loppuTunti.clear();
                }
            } 
        }
    }
    @FXML
    private void timeWriteActionLoppuHour(KeyEvent event) {
        if(event.getSource()instanceof TextField){
            TextField muokattava =(TextField) event.getSource();
            if(muokattava.getText().length() > 2 || !isNumeric(muokattava.getText())) {
                muokattava.clear();
            }
            if (muokattava.getText().length() == 2){
                if(Integer.parseInt(muokattava.getText()) > 23 ) {
                    muokattava.setText("23");
                    loppuMinuutti.requestFocus();
                    loppuMinuutti.clear();
                } else if (Integer.parseInt(muokattava.getText()) < Integer.parseInt(alkuTunti.getText())){
                    muokattava.setText(alkuTunti.getText());
                    loppuMinuutti.requestFocus();
                    loppuMinuutti.clear();
                } else {
                    loppuMinuutti.requestFocus();
                    loppuMinuutti.clear();
                }
            } 
        }
    }
    @FXML
    private void timeWriteActionLoppuMinute(KeyEvent event) {
        if(event.getSource()instanceof TextField){
            TextField muokattava =(TextField) event.getSource();
            if(muokattava.getText().length() > 2 || !isNumeric(muokattava.getText())) {
                muokattava.clear();
            }
            if (muokattava.getText().length() == 2){
                if(Integer.parseInt(muokattava.getText()) > 59) {
                    muokattava.setText("59");
                    listMoviesButton.requestFocus();
                } else if (Integer.parseInt(muokattava.getText()) < Integer.parseInt(alkuMinuutti.getText()) && loppuTunti.getText().equals(alkuTunti.getText())){
                    muokattava.setText(alkuMinuutti.getText());
                    listMoviesButton.requestFocus();
                } else {
                    listMoviesButton.requestFocus();
                }
            } 
        }
    }
    
    private boolean isNumeric(String tsek){
        try {
            int i = Integer.parseInt(tsek);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
    
    @FXML
    private void searchButtonAction(ActionEvent event) {
        Elokuvalista elokuvaLista = new Elokuvalista();
        String paiva = "";
        if (dayList.getText().length() == 1) {
            paiva = "0";
        }
        paiva += dayList.getText();
        
        String kuukausi = "";
        if (monthList.getText().length() == 1) {
            kuukausi = "0";
        }
        kuukausi += monthList.getText();
        String currentDay = paiva+"."+kuukausi+"."+yearList.getText();
        resultList.setItems(elokuvaLista.getTheatresByMovie(currentDay, teatteriListaBox.getValue().getID(), nimiHakuField.getText()));
        nimiHakuField.setText(((TeatteriElokuva)resultList.getItems().get(0)).getName());
    }



}
