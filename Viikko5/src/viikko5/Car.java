/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko5;

import java.util.ArrayList;

/**
 *
 * @author Lassi Nieminen
 *          IT-Kesäleiri 2018 Olio-ohjelmointia
 */
public class Car {
    private Moottori moottori;
    private Alusta alusta;
    private Kori kori;
    private ArrayList<Rengas> renkaat;
    
    public Car() {
        kori = new Kori();
        alusta = new Alusta();
        moottori = new Moottori();
        renkaat = new ArrayList();
        for (int i = 0 ; i < 4 ; i++) {
            renkaat.add(new Rengas());
        }
    }
    
    public class Alusta {
        
        public Alusta() {
            System.out.println("Valmistetaan: Chassis");
        }
    }
    
    public class Rengas {
        
        public Rengas() {
            System.out.println("Valmistetaan: Wheel");
        }
    }
    
    public class Kori {
        
        public Kori() {
            System.out.println("Valmistetaan: Body");
        }
    }
    
    public class Moottori {
        
        public Moottori() {
            System.out.println("Valmistetaan: Engine");
        }   
    }
    
    public void print() {
        System.out.println("Autoon kuuluu:");
        System.out.println("\tBody");
        System.out.println("\tChassis");
        System.out.println("\tEngine");
        System.out.println("\t4 Wheel");
    }
}
