/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko5;

import java.util.Scanner;

/**
 *
 * @author Lassi Nieminen
 */

/* Car.javan kanssa toimiva Mainclass 
public class Mainclass {

    public static void main(String[] args) {
        Car a = new Car();
        a.print();
    }
    
}
*/

public class Mainclass {
    public static void main(String[] args) {
        Character y = null;
        boolean hahmoluotu = false;
        OUTER:
        while (true) {
            System.out.println("*** TAISTELUSIMULAATTORI ***");
            System.out.println("1) Luo hahmo");
            System.out.println("2) Taistele hahmolla");
            System.out.println("0) Lopeta");
            System.out.print("Valintasi: ");
            Scanner scan = new Scanner(System.in);
            int valinta = scan.nextInt();
            switch (valinta) {
                case 1:
                    System.out.println("Valitse hahmosi:");
                    System.out.println("1) Kuningas");
                    System.out.println("2) Ritari");
                    System.out.println("3) Kuningatar");
                    System.out.println("4) Peikko");
                    System.out.print("Valintasi: ");
                    scan = new Scanner(System.in);
                    valinta = scan.nextInt();
                    if (valinta == 1) {
                        y = new King();
                        hahmoluotu = true;
                    } else if (valinta == 2) {
                        y = new Knight();
                        hahmoluotu = true;
                    } else if (valinta == 3) {
                        y = new Queen();
                        hahmoluotu = true;
                    } else if (valinta == 4) {
                        y = new Troll();
                        hahmoluotu = true;
                    } else {
                        System.out.println("Ei");
                    }   break;
                case 2:
                    if (hahmoluotu){
                        y.fight();
                    } else {
                        System.out.println("Ei");
                    }
                    break;
                case 0:
                    break OUTER;
                default:
                    System.out.println("Ei");
                    break;
            }
        }
    }
}
