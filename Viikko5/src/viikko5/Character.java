/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package viikko5;

import java.util.Scanner;

/**
 *
 * @author Lassi Nieminen
 *          IT-Kesäleiri 2018 Olio-ohjelmointia
 */
public abstract class Character {
    
    public WeaponBehavior weapon;
    
    public Character(){
        System.out.println("Valitse aseesi: ");
        System.out.println("1) Veitsi \n2) Kirves \n3) Miekka \n4) Nuija");
        System.out.print("Valintasi: ");
        Scanner scan = new Scanner(System.in);
        int valinta = scan.nextInt();
        if (valinta == 1) {
            weapon = new KnifeBehavior();
        } else if (valinta == 2) {
            weapon = new AxeBehavior();
        } else if (valinta == 3) {
            weapon = new SwordBehavior();
        } else if (valinta == 4) {
            weapon = new ClubBehavior();
        } else {
            System.out.println("Ei");
        }
    }
    
    public abstract void fight();
}

class King extends Character {
    
    @Override
    public void fight() {
        System.out.println(this.getClass().getSimpleName() + " tappelee aseella " + this.weapon.useWeapon());
    }
}
class Knight extends Character {
    
    @Override
    public void fight() {
        System.out.println(this.getClass().getSimpleName() + " tappelee aseella " + this.weapon.useWeapon());
    }
}
class Queen extends Character {

    @Override
    public void fight() {
        System.out.println(this.getClass().getSimpleName() + " tappelee aseella " + this.weapon.useWeapon());
    }
}
class Troll extends Character {

    @Override
    public void fight() {
        System.out.println(this.getClass().getSimpleName() + " tappelee aseella " + this.weapon.useWeapon());
    }
}

abstract class WeaponBehavior {
    
    abstract String useWeapon();
}

class SwordBehavior extends WeaponBehavior {
    
    @Override
    public String useWeapon() {
        return "Sword";
    }
}

class AxeBehavior extends WeaponBehavior {
    
    @Override
    public String useWeapon() {
        return "Axe";
    }
}

class KnifeBehavior extends WeaponBehavior {
    
    @Override
    public String useWeapon() {
        return "Knife";
    }
}

class ClubBehavior extends WeaponBehavior {
    
    @Override
    public String useWeapon() {
        return "Club";
    }
}