/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package viikko8;

/**
 *
 * @author Lassi Nieminen
 *          IT-Kesäleiri 2018 Olio-ohjelmointia
 */
public class Bottle {
    String nimi;
    double hinta;
    String koko;
    
    public Bottle(){
        nimi = "Pepe Max";
        hinta = 1.8;
        koko = "0.5";
    }
    
    public Bottle(String name, double price, String size) {
        nimi = name;
        hinta = price;
        koko = size;
    }
    
    public double getHinta(){
        return hinta;
    }
    
    public String getNimi(){
        return nimi;
    }
    
    public String getKoko(){
        return koko;
    }

}