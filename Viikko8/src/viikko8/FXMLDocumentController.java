/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko8;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;

/**
 *
 * @author p9042
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label rahalabel;
    @FXML
    private Label moneyAmount;
    @FXML
    private TextArea monitori;
    @FXML
    private Slider rahaSlider;
    @FXML
    private ComboBox pulloNimi;
    @FXML
    private ComboBox pulloKoko;
    
    BottleDispenser pullokone = BottleDispenser.luoPullokone();
    
    
    @FXML
    private void addMoneyAction(ActionEvent event) {
        pullokone.lisaaRahaa(rahaSlider.getValue());
        rahaSlider.setValue(0);
        moneyAmount.setText(String.format("%.2f",pullokone.kerroRahat()));
    }
    
    @FXML
    private void returnMoneyAction(ActionEvent event) {
        pullokone.palautaRahat();
        moneyAmount.setText(String.format("%.2f",pullokone.kerroRahat()));
    }
    
    @FXML
    private void ostaPulloAction(ActionEvent event) {
        if(pulloNimi.getValue() != null && pulloKoko.getValue() != null) {
            if(pullokone.ostaPullo(pulloNimi.getValue().toString(), pulloKoko.getValue().toString()) == 1){
                monitori.setText(pullokone.kerroPullot());
                moneyAmount.setText(String.format("%.2f",pullokone.kerroRahat()));
            }    
        }
    }
    
    @FXML
    private void kuittiAction(ActionEvent event) {
        if (pullokone.getViimeisinPullo() != null) {
            try {
                BufferedWriter out = new BufferedWriter(new FileWriter("kuitti.txt"));
                String kuitti = String.format("%s\t%s litraa\t %.2f€",pullokone.getViimeisinPullo().getNimi(),pullokone.getViimeisinPullo().getKoko(), pullokone.getViimeisinPullo().getHinta());
                out.write(kuitti);
                out.close();
            } catch (FileNotFoundException ex){
                System.err.println("File not found.");
            } catch (IOException i) {
                System.err.println("IOException");
            }
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        monitori.setText(pullokone.kerroPullot());
    }    
    
}
