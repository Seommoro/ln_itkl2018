/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko8;

/**
 *
 * @author Lassi Nieminen
 */
import java.util.ArrayList;
import java.util.Iterator;

public class BottleDispenser {
    private static int pullot;
    private static double rahaa;
    private static ArrayList<Bottle> bottles;
    private static final BottleDispenser pullokone = new BottleDispenser();
    private static Bottle viimeisinPullo;
    
    private BottleDispenser(){
        pullot = 6;
        rahaa = 0;
        bottles = new ArrayList();
        bottles.add(new Bottle("Pepe Max", 1.8, "0.5")); 
        bottles.add(new Bottle("Pepe Max", 2.2, "1.5"));
        bottles.add(new Bottle("Trump-Wall Zero", 2.0, "1.5"));
        bottles.add(new Bottle("Trump-Wall Zero", 2.5, "1.5"));
        bottles.add(new Bottle("Kek Zero", 1.95, "0.5"));
        bottles.add(new Bottle("Kek Zero", 1.95, "0.5"));
        viimeisinPullo = null;
    }
    
    public void lisaaRahaa(double x){
        rahaa += x;
        System.out.println("Klink! Lisää rahaa laitteeseen! " + x);
    }
    
    public int ostaPullo(String pullovalinta, String pullokoko) {
        Iterator<Bottle> i = bottles.iterator();
        while(i.hasNext()) {
            Bottle s = i.next();
            if (s.getNimi().equals(pullovalinta) && s.getKoko().equals(pullokoko)){
                if (rahaa == 0 || rahaa < s.getHinta()) {
                    System.out.println("Syötä rahaa ensin!");
                    return 0;
                } else if (pullot == 0) {
                    System.out.println("Ei ole pulloja jäljellä!)");
                    return 0;
                } else {
                    System.out.println("KACHUNK! " + s.getNimi() + " tipahti masiinasta!");
                    rahaa -= s.getHinta();
                    pullot -= 1;
                    viimeisinPullo = s;
                    i.remove();
                    return 1;
                }
            }
        }
        return 0;
       /* for(Bottle pullo : bottles) {
            if (pullo.getNimi().equals(pullovalinta)){
                if (rahaa == 0 || rahaa < pullo.getHinta()) {
                    System.out.println("Syötä rahaa ensin!");
                    return 0;
                } else if (pullot == 0) {
                    System.out.println("Ei ole pulloja jäljellä!)");
                    return 0;
                } else {
                    System.out.println("KACHUNK! " + bottles.get(pullovalinta).getNimi() + " tipahti masiinasta!");
                    rahaa -= bottles.get(pullovalinta).getHinta();
                    pullot -= 1;
                    bottles.remove(pullovalinta);
                    return 1;
                }
            } else {
                return 0;
            }
        }
        if (rahaa == 0 || rahaa < bottles.get(pullovalinta).getHinta()) {
            System.out.println("Syötä rahaa ensin!");
            return 0;
        } else if (pullot == 0) {
            System.out.println("Ei ole pulloja jäljellä!)");
            return 0;
        } else {
            System.out.println("KACHUNK! " + bottles.get(pullovalinta).getNimi() + " tipahti masiinasta!");
            rahaa -= bottles.get(pullovalinta).getHinta();
            pullot -= 1;
            bottles.remove(pullovalinta);
            return 1;
        }*/
    }
    
    public void palautaRahat() {
        if (rahaa == 0) {
            System.out.println("Ei ole rahaa mitä palauttaa!");
            
        } else {
            String palaute = String.format("Klink klink. Sinne menivät rahat! Rahaa tuli ulos %.2f", rahaa);
            rahaa = 0;
            System.out.println(palaute);
        }
    }
    
    public String kerroPullot() {
        String palaute = "";
        for (int i = 0; i < pullot; i++) {
            palaute = palaute + (i+1) + ". Nimi: " + bottles.get(i).getNimi()+ "\n\tKoko: " + String.valueOf(bottles.get(i).getKoko()) + "\tHinta: " + String.valueOf(bottles.get(i).getHinta()+"\n");
        }
        return palaute;
    }
    
    public static double kerroRahat(){
        return rahaa;
    }
    
    public static BottleDispenser luoPullokone(){
        return pullokone;
    }
    
    public static Bottle getViimeisinPullo(){
        return viimeisinPullo;
    }
}
