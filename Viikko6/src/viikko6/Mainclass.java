/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package viikko6;

import java.util.Scanner;

/**
 *
 * @author Lassi Nieminen
 *          IT-Kesäleiri 2018 Olio-Ohjelmointia
 */

public class Mainclass {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String tilinumero;
        int rahamaara;
        int luottoraja;
        Bank bank = new Bank();
        OUTER:
        while (true) {
            System.out.println("\n*** PANKKIJÄRJESTELMÄ ***\n"
                    + "1) Lisää tavallinen tili\n"
                    + "2) Lisää luotollinen tili\n"
                    + "3) Tallenna tilille rahaa\n"
                    + "4) Nosta tililtä\n"
                    + "5) Poista tili\n"
                    + "6) Tulosta tili\n"
                    + "7) Tulosta kaikki tilit\n"
                    + "0) Lopeta");
            System.out.print("Valintasi: ");
            int valinta = scan.nextInt();
            switch (valinta) {
                case 1:
                    System.out.print("Syötä tilinumero: ");
                    tilinumero = scan.next();
                    System.out.print("Syötä rahamäärä: ");
                    rahamaara = scan.nextInt();
                    bank.addTili(1, tilinumero, rahamaara);
                    break;
                case 2:
                    System.out.print("Syötä tilinumero: ");
                    tilinumero = scan.next();
                    System.out.print("Syötä rahamäärä: ");
                    rahamaara = scan.nextInt();
                    System.out.print("Syötä luottoraja: ");
                    luottoraja = scan.nextInt();
                    bank.addTili(2, tilinumero, rahamaara, luottoraja);
                    break;
                case 3:
                    System.out.print("Syötä tilinumero: ");
                    tilinumero = scan.next();
                    System.out.print("Syötä rahamäärä: ");
                    rahamaara = scan.nextInt();
                    bank.addRaha(tilinumero, rahamaara);
                    break;
                case 4:
                    System.out.print("Syötä tilinumero: ");
                    tilinumero = scan.next();
                    System.out.print("Syötä rahamäärä: ");
                    rahamaara = scan.nextInt();
                    bank.nostaRaha(tilinumero, rahamaara*-1);
                    break;
                case 5:
                    System.out.print("Syötä poistettava tilinumero: ");
                    tilinumero = scan.next();
                    bank.removeTili(tilinumero);
                    break;
                case 6:
                    System.out.print("Syötä tulostettava tilinumero: ");
                    tilinumero = scan.next();
                    bank.tulostaTili(tilinumero);
                    break;
                case 7:
                    bank.tulostaKaikki();
                    break;
                case 0:
                    break OUTER;
                default:
                    System.out.println("Valinta ei kelpaa.");
            }
       }
    } 
}
