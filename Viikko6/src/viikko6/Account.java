/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package viikko6;

/**
 *
 * @author Lassi Nieminen
 *          IT-Kesäleiri 2018 Olio-ohjelmointia
 */
public abstract class Account {
    protected String tiliNumero;
    protected int rahamaara;
    
    public Account(){
        
    }
    public String getNumero(){
        return tiliNumero;
    }
    
    public int getRaha(){
        return rahamaara; 
    }
    
    public abstract boolean setRaha(int uusiRaha);
}

class BasicTili extends Account {
    
    public BasicTili(String tiliNmr, int rahaMr) {
        tiliNumero = tiliNmr;
        rahamaara = rahaMr;
    }
    
    @Override
    public boolean setRaha(int uusiRaha) {
        if (uusiRaha + rahamaara > 0) {
            rahamaara += uusiRaha;
            return true;
        } else {
            System.out.println("Ei tarpeeksi rahaa");
            return false;
        }
    }
}

class LuottoTili extends Account {
    
    private int luotto;
    
    public LuottoTili(String tiliNmr, int rahaMr, int luot){
        tiliNumero = tiliNmr;
        rahamaara = rahaMr;
        luotto = luot;
    }
    
    @Override
    public boolean setRaha(int uusiRaha) {
        if (luotto + uusiRaha + rahamaara > 0) {
            rahamaara += uusiRaha;
            return true;
        } else {
            return false;
        }
    }
    
    public int getLuotto() {
        return luotto;
    }
}
