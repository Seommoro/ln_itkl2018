/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko6;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author Lassi Nieminen
 *          IT-Kesäleiri 2018 Olio-ohjelmointia
 */
public class Bank {
    
    //private String pankkiNimi;
    private Map<String, Account> tilit;
    
    public Bank() {
        tilit = new HashMap();
    }
    
    public void addTili(int tyyppi, String tiliNumero, int rahaMaara, int luotto) {
        if (tyyppi == 1) {
            tilit.put(tiliNumero, new BasicTili(tiliNumero, rahaMaara));
            System.out.println("Tili luotu.");
        } else if (tyyppi == 2) {
            tilit.put(tiliNumero, new LuottoTili(tiliNumero, rahaMaara, luotto));
            System.out.println("Tili luotu.");
        }
    }
    
    public void addTili(int tyyppi, String tiliNumero, int rahaMaara) {
        addTili(tyyppi, tiliNumero, rahaMaara, -1);
    }
        
    public void addRaha(String tiliNumero, int rahaMaara){
        if (tilit.containsKey(tiliNumero)){
            tilit.get(tiliNumero).setRaha(rahaMaara);
        } else {
            System.out.println("Tiliä ei ole olemassa.");
        }
    }
    
    public void nostaRaha(String tiliNumero, int rahaMaara) {
        if (tilit.containsKey(tiliNumero)){
            if(!tilit.get(tiliNumero).setRaha(rahaMaara)){
                System.out.println("Tilillä ei ole tarpeeksi rahaa.");
            } 
        } else {
            System.out.println("Tiliä ei ole olemassa.");
        }
    }
    
    public void tulostaKaikki() {
        System.out.println("Kaikki tilit:");
        for (Map.Entry<String, Account> tili : tilit.entrySet()) {
            if (tili.getValue() instanceof BasicTili) {
                System.out.println("Tilinumero: " + tili.getValue().getNumero() + " Tilillä rahaa: " + tili.getValue().getRaha());
            } else if (tili.getValue() instanceof LuottoTili) {
                LuottoTili ltili = (LuottoTili)tili.getValue();
                System.out.println("Tilinumero: " + tili.getValue().getNumero() + " Tilillä rahaa: " + tili.getValue().getRaha() + " Luottoraja: " + ltili.getLuotto());
            }
            
        }
    }
    
    public void tulostaTili(String tiliNumero) {
        if (tilit.containsKey(tiliNumero)){
            System.out.println("Tilinumero: " + tilit.get(tiliNumero).getNumero() + " Tilillä rahaa: " + tilit.get(tiliNumero).getRaha());
        } else {
            System.out.println("Tiliä ei ole olemassa.");
        }
    }
    
    public void removeTili(String tiliNumero){
        if (tilit.containsKey(tiliNumero)){
            tilit.remove(tiliNumero);
            System.out.println("Tili poistettu.");
        } else {
            System.out.println("Tiliä ei ole olemassa.");
        }
    }
}