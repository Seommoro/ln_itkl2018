/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko3;

import java.util.Scanner;

/**
 *
 * @author Lassi Nieminen
 */
public class MainClass {
    
    public static void main(String[] args) {
        BottleDispenser pullokone = new BottleDispenser();
        OUTER:
        while (true) {
            System.out.println("\n*** LIMSA-AUTOMAATTI ***");
            System.out.println("1) Lisää rahaa koneeseen\n2) Osta pullo\n3) Ota rahat ulos\n4) Listaa koneessa olevat pullot\n0) Lopeta");
            System.out.print("Valintasi: ");
            Scanner scan = new Scanner(System.in);
            String valinta = scan.nextLine();
            switch (valinta) {
                case "1":
                    pullokone.lisaaRahaa();
                    break;
                case "2":
                    pullokone.kerroPullot();
                    System.out.print("Valintasi: ");
                    Scanner scanpullo = new Scanner(System.in);
                    if (scanpullo.hasNextInt()) {
                        pullokone.ostaPullo(scanpullo.nextInt()-1);
                    } else {
                        System.out.println("Paska valinta.");
                    }   break;
                case "3":
                    pullokone.palautaRahat();
                    break;
                case "4":
                    pullokone.kerroPullot();
                    break;
                case "0":
                    break OUTER;
                default:
                    System.out.println("Tuntematon valinta.");
                    break;
            }
        }
    }
    
}
