/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko3;

/**
 *
 * @author Lassi Nieminen
 */
import java.util.ArrayList;
import java.util.List;

public class BottleDispenser {
    private int pullot;
    private double rahaa;
    private List<Bottle> bottles;
    
    public BottleDispenser(){
        pullot = 6;
        rahaa = 0;
        bottles = new ArrayList();
        /*for (int i = 0; i < pullot; i++) {
            bottles.add(new Bottle());
        }*/
        bottles.add(new Bottle("Pepsi Max", 1.8, 0.5));
        bottles.add(new Bottle("Pepsi Max", 2.2, 1.5));
        bottles.add(new Bottle("Coca-Cola Zero", 2.0, 1.5));
        bottles.add(new Bottle("Coca-Cola Zero", 2.5, 1.5));
        bottles.add(new Bottle("Fanta Zero", 1.95, 0.5));
        bottles.add(new Bottle("Fanta Zero", 1.95, 0.5));
    }
    
    public void lisaaRahaa(){
        rahaa += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }
    
    public void ostaPullo(int pullovalinta) {
        if (rahaa == 0 || rahaa < bottles.get(pullovalinta).getHinta()) {
            System.out.println("Syötä rahaa ensin!");
        } else if (pullot == 0) {
            System.out.println("Ei ole pulloja jäljellä!)");
        } else {
            System.out.println("KACHUNK! " + bottles.get(pullovalinta).getNimi() + " tipahti masiinasta!");
            rahaa -= bottles.get(pullovalinta).getHinta();
            pullot -= 1;
            bottles.remove(pullovalinta);
        }
    }
    
    public void palautaRahat() {
        if (rahaa == 0) {
            System.out.println("Ei ole rahaa mitä palauttaa!");
        } else {
            System.out.printf("Klink klink. Sinne menivät rahat! Rahaa tuli ulos %.2f€\n", rahaa);
        }
    }
    
    public void kerroPullot() {
        for (int i = 0; i < pullot; i++) {
            System.out.println((i+1) + ". Nimi: " + bottles.get(i).getNimi());
            System.out.println("\tKoko: " + String.valueOf(bottles.get(i).getKoko()) + "\tHinta: " + String.valueOf(bottles.get(i).getHinta()));
        }
    }
}
