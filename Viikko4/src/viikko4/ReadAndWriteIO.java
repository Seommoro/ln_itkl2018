package viikko4;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
/**
 *
 * @author Lassi Nieminen
 *          IT-Kesäleiri 2018 Olio-ohjelmointia
 */
public class ReadAndWriteIO {
    
    public ReadAndWriteIO() {
        
}
    public void readFile(String tiedostopolku) {
        try {
            BufferedReader in = new BufferedReader(new FileReader(tiedostopolku));
            String tulostus = in.readLine();
            while(tulostus != null) {
            System.out.println(tulostus);
            tulostus = in.readLine();
            }
            in.close();
        } catch (FileNotFoundException ex){
            System.err.println("File not found.");
        } catch (IOException i) {
            System.err.println("IOException");
        }
    }
    
    public void ReadAndWrite(String inPolku, String outPolku) {
        try {
            BufferedReader in = new BufferedReader(new FileReader(inPolku));
            BufferedWriter out = new BufferedWriter(new FileWriter(outPolku));
            String tulostus;
            while((tulostus = in.readLine()) != null){
                if (tulostus.trim().length() < 30 && tulostus.trim().length() != 0) {
                    for(int i = 0; i < tulostus.length(); i++) {
                        if (tulostus.charAt(i) == 'v' || tulostus.charAt(i) == 'V') {
                            out.write(tulostus + "\n");
                        }
                    }
                }
            }
            in.close();
            out.close();
        } catch (FileNotFoundException ex){
            System.err.println("File not found.");
        } catch (IOException i) {
            System.err.println("IOException");
        }
    }
    
    public void ReadZip(String inPolku) {
        try {
            ZipFile inZip = new ZipFile("zipinput.zip");
            
            ZipEntry zipRead = inZip.entries().nextElement();
            InputStream in = inZip.getInputStream(zipRead);
            BufferedReader inRead = new BufferedReader(new InputStreamReader(in));
            String tulostus;
            while((tulostus = inRead.readLine()) != null) {
            System.out.println(tulostus);
            }
            inZip.close();
            in.close();
            inRead.close();
        } catch (FileNotFoundException ex){
            System.err.println("File not found.");
        } catch (IOException i) {
            System.err.println("IOException");
        }
    }
}
