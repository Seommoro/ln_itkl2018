
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 *
 * @author p9042
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    
    @FXML
    private TextArea tekstiKentta;
    
    @FXML 
    private TextField tiedostoKentta;
    
    @FXML
    private void avaaNappiAction(ActionEvent event){
        try {
            BufferedReader in = new BufferedReader(new FileReader(System.getProperty("user.dir") + "/"+tiedostoKentta.getText()));
            tekstiKentta.setText("");
            String tulostus;
            while((tulostus = in.readLine()) != null) {
            tekstiKentta.setText(tekstiKentta.getText() + tulostus+"\n");
            }
            in.close();
        } catch (FileNotFoundException ex){
            System.err.println("File not found.");
        } catch (IOException i) {
            System.err.println("IOException");
        }
        
    }
    
    @FXML
    private void tallennaNappiAction(ActionEvent event) {
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(System.getProperty("user.dir") + "/"+tiedostoKentta.getText()));
            out.write(tekstiKentta.getText());
            out.close();
        } catch (FileNotFoundException ex){
            System.err.println("File not found.");
        } catch (IOException i) {
            System.err.println("IOException");
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        label.setText("JeeJee");
    }    
    
}
