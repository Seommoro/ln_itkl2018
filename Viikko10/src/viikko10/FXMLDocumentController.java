/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko10;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.Font;
import javafx.scene.web.WebView;

/**
 *
 * @author p9042
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private TextField urlField;
    @FXML
    private Font x1;
    @FXML
    private WebView webBox;
    
    private String currentPage;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        webBox.getEngine().load("http://www.google.com");
        currentPage = "http://www.google.com";
    }    

    @FXML
    private void refreshPageAction(ActionEvent event) {
        webBox.getEngine().reload();
    }

    @FXML
    private void loadPageAction(ActionEvent event) {
        if (urlField.getText().equalsIgnoreCase("index.html")) {
            webBox.getEngine().load(getClass().getResource("index.html").toExternalForm());
            return;
        }
        String newPage;
        if (!(urlField.getText().toLowerCase().startsWith("http://") || urlField.getText().toLowerCase().startsWith("https://"))) {
            newPage = "http://"+urlField.getText();
        } else {
            newPage = urlField.getText();
        }
        if (getResponseCode(newPage) == 200  || getResponseCode(newPage) == 301 || getResponseCode(newPage) == 302){
            webBox.getEngine().load(newPage);
            urlField.setText(newPage);
            currentPage = newPage;
            
        } else {
            webBox.getEngine().load("http://www.google.com/search?q=" + urlField.getText());
            currentPage = "http://www.google.com/search?q=" + urlField.getText();
            urlField.setText(currentPage);
        }
    }

    @FXML
    private void loadPreviousPageAction(ActionEvent event) {
    }

    @FXML
    private void loadNextPageAction(ActionEvent event) {
    }
    
    
    private int getResponseCode(String urlString){
        try {
            URL url = new URL(urlString);
            HttpURLConnection huc = (HttpURLConnection) url.openConnection();
            huc.setRequestMethod("GET");
            huc.connect();
            System.out.print("1.");
            System.out.println(huc.getResponseCode());
            return huc.getResponseCode();
        } catch (MalformedURLException monsterUrl) {
            System.out.println("asdasdasd");
        } catch (IOException ex) {
            return -1;
        }
        return 123;
    }

    @FXML
    private void initializeButton(ActionEvent event) {
        webBox.getEngine().executeScript("initialize()");
    }

    @FXML
    private void shoutoutButton(ActionEvent event) {
        webBox.getEngine().executeScript("document.shoutOut()");
    }
}
